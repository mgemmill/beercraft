import pytest
from beercraft.event import EventError, PubSubError


#  def create_error_handler(obj):
#      def error_handler(ex):
#          obj.global_variable[0] += 'ERRORCAPTURE: '
#      return error_handler


def test_event_has_callable(objects):
    assert objects.call_a.method in objects.event
    assert objects.call_b.method in objects.event
    assert objects.call_c.method in objects.event


def test_eid(objects):
    assert objects.event.eid == "test event"


def test_event_notification(objects):
    objects.event.notify()
    assert objects.call_a.global_variable == ["AAABBBCCC"]
    assert objects.call_b.global_variable == ["AAABBBCCC"]
    assert objects.call_c.global_variable == ["AAABBBCCC"]


def test_event_register_invalid_method(objects):
    class A(object):
        def unbound_method(self):
            pass

    with pytest.raises(PubSubError):
        objects.event.register(A.unbound_method)


def test_event_unregister_method(objects):
    objects.event.unregister(objects.call_b.method)
    objects.event.notify()

    assert objects.call_a.global_variable == ["AAACCC"]
    assert objects.call_b.global_variable == ["AAACCC"]
    assert objects.call_c.global_variable == ["AAACCC"]


def test_event_clear(objects):
    objects.event.clear()
    objects.event.notify()

    assert objects.call_a.global_variable == [""]
    assert objects.call_b.global_variable == [""]
    assert objects.call_c.global_variable == [""]


def test_deleted_reference(objects):
    del objects.call_a
    objects.event.notify()

    assert objects.call_b.global_variable == ["BBBCCC"]
    assert objects.call_c.global_variable == ["BBBCCC"]


def test_nullified_reference(objects):
    objects.call_b = None
    objects.event.notify()

    assert objects.call_a.global_variable == ["AAACCC"]
    assert objects.call_c.global_variable == ["AAACCC"]


def test_event_notification_with_arguments(objargs):
    objargs.event("ZZZ")
    assert objargs.call_a.global_variable == ["ZZZZZZZZZ"]
    assert objargs.call_b.global_variable == ["ZZZZZZZZZ"]
    assert objargs.call_c.global_variable == ["ZZZZZZZZZ"]


def test_event_unregister_with_minus_equal_operator(objargs):
    objargs.event -= objargs.call_a.method
    objargs.event("ZZZ")
    assert objargs.call_a.global_variable == ["ZZZZZZ"]
    assert objargs.call_b.global_variable == ["ZZZZZZ"]
    assert objargs.call_c.global_variable == ["ZZZZZZ"]


def test_event_iter_ref(objects):
    for method in objects.event._iter_ref():
        assert type(method).__name__ in ("weakref", "WeakMethod")


def test_event_iter_callable(objects):
    for method in objects.event._iter_callables():
        # these are all NewCallableObject.method bound methods
        assert type(method).__name__ == "method"


def test_event_error_raised(objerr):
    with pytest.raises(EventError):
        objerr.event()


def test_event_execution_halted_on_error(objerr):
    try:
        objerr.event()
    except Exception as ex:  # noqa
        assert str(ex) == "EventError: Exception was raised!"

    assert objerr.call_a.global_variable == [""]
    assert objerr.call_b.global_variable == [""]
    assert objerr.call_c.global_variable == [""]


def test_event_execution_continue_on_error(objerr):
    objerr.event.halt_on_error = False
    assert objerr.event.halt_on_error is False

    try:
        objerr.event()
    except:  # noqa
        pass

    assert objerr.call_a.global_variable == [""]
    assert objerr.call_b.global_variable == [""]
    assert objerr.call_c.global_variable == [""]


def test_event_set_error_handler(objerr):
    objerr.event.set_error_handler(objerr.error_handler)
    assert objerr.event._errors.handler == objerr.error_handler


def test_event_custom_error_handler(objerr):
    objerr.event.set_error_handler(objerr.error_handler)
    try:
        objerr.event()
    except:  # noqa
        pass

    assert objerr.call_a.global_variable == ["ERRORCAPTURE: "]
    assert objerr.call_b.global_variable == ["ERRORCAPTURE: "]
    assert objerr.call_c.global_variable == ["ERRORCAPTURE: "]


def test_event_execution_continue_on_custom_error_handler(objerr):
    objerr.event.halt_on_error = False
    objerr.event.set_error_handler(objerr.error_handler)
    try:
        objerr.event()
    except:  # noqa
        pass

    assert objerr.call_a.global_variable == ["ERRORCAPTURE: BBBCCC"]
    assert objerr.call_b.global_variable == ["ERRORCAPTURE: BBBCCC"]
    assert objerr.call_c.global_variable == ["ERRORCAPTURE: BBBCCC"]
