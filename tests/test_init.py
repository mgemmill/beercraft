import pytest
import beercraft
from beercraft.types import PubSubError
from . import tmodule
from .tutils import Commands, ClassCommands


def test_hub_singleton():
    hub1 = beercraft.Hub()
    hub2 = beercraft.Hub()
    assert hub1 is hub2


def test_hub_usage_class_instance():

    hub = beercraft.Hub()
    hub.clear_all()

    cmd = Commands()

    beercraft.subscribe_commands(cmd)

    hub.send_message("commands.call")

    assert cmd.global_variable == ["AAABBBCCC"]


def test_hub_usage_module():

    hub = beercraft.Hub()
    hub.clear_all()

    beercraft.subscribe_commands(tmodule)

    hub.send_message("module.call")
    assert tmodule.global_variable == ["AAABBBCCC"]

    tmodule.clear_global()

    hub.send_message("module.call", value="ZZZ")
    assert tmodule.global_variable == ["ZZZZZZZZZ"]


def test_subscribe_commands():

    hub = beercraft.Hub()
    hub.clear_all()

    beercraft.subscribe_commands(ClassCommands)


def test_hub_send_message_to_unbound_class_method():

    hub = beercraft.Hub()
    hub.clear_all()

    beercraft.subscribe_commands(Commands)

    with pytest.raises(PubSubError):
        hub.send_message("commands.call")
