import inspect


class Foo(object):
    def __init__(self):
        pass

    def method(self):
        pass

    @classmethod
    def classmethod(clx):
        pass

    @staticmethod
    def staticmethod():
        pass


class Bar(object):
    def __call__(self):
        pass


def func():
    pass


foo = Foo()
bar = Bar()


def bound(obj):
    if hasattr(obj, "__self__"):
        return obj.__self__ is not None
    return False


def object_type(obj):
    obj_type = ""
    if inspect.isclass(obj):
        obj_type = "class"

    if inspect.isfunction(obj):
        obj_type = "function"

    if inspect.ismethod(obj):
        obj_type = "method"

    return obj_type


def has_self_arg(obj):
    return callable(obj) and "self" in inspect.getargspec(obj).args


is_callable = lambda o: "callable" if callable(o) else "non-callable"

is_bound = lambda o: "bound" if bound(o) else "unbound"

is_object = lambda o: "is object" if isinstance(o, object) else "not an object"

is_type = lambda o: "is type" if isinstance(o, type) else "not a type"

has_self = lambda o: "has self" if hasattr(o, "__self__") else ""

has_qualify = lambda o: o.__qualname__ if hasattr(o, "__qualname__") else ""

module_name = lambda o: inspect.getmodule(o).__name__

is_class = lambda o: "class" if inspect.isclass(o) else ""

is_method = lambda o: "is method" if has_self_arg(o) else "is function"


def determinations(obj, description):
    return [
        description,  # description
        str(obj),  # str
        str(type(obj)),  # type
        type(obj).__name__,  # type_name
        object_type(obj),  # obj_type
        is_callable(obj),  # callable
        is_bound(obj),  # bound
        module_name(obj),  # module
        is_object(obj),  # object
        is_type(obj),  # type
        has_self(obj),  # self
        has_qualify(obj),  # quanlify
        is_class(obj),  # class
        is_method(obj),  # method
    ]


"""

These do not require weak references:

    plain functions in a module
    class method of a class or class instance
    static function of a class

These do require weak references:
    bound method of a class instance
    class instance that is callable

We need to be able to identify these....

"""

objects = [
    (func, "yes,function, func function"),
    (Foo, "no,class, Foo class"),
    (foo, "no,instance, Foo instance"),
    (Foo.method, "no,class, Foo.method"),
    (Foo.classmethod, "yes,class, Foo.classmethod"),
    (Foo.staticmethod, "yes,class, Foo.staticmethod"),
    (foo.method, "yes,instance, foo.method"),
    (foo.classmethod, "yes,instance, foo.classmethod"),
    (foo.staticmethod, "yes,instance, foo.staticmethod"),
    (Bar, "no,class, Bar class"),
    (bar, "yes,instance, Bar instance"),
    (Bar.__call__, "no,class, Bar.__call__"),
    (bar.__call__, "yes,instance, bar.__call__"),
]


headers = (
    "acceptable,state,object,str,type,type_name,obj_type,"
    "callable,bound,module,obj instance,"
    "type instance,self,qualname,is_class,is_method\n"
)


with open("object-info.csv", "w") as fh_:
    fh_.write(headers)
    for obj, name in objects:
        fh_.write(",".join(determinations(obj, name)))
        fh_.write("\n")
