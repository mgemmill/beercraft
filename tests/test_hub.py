import pytest
from beercraft.types import PubSubError
from beercraft.event import EventError


def test_hub_bad_subscription_calls(hubtst):

    with pytest.raises(PubSubError):
        hubtst.hub.subscribe("topic.one", hubtst.call_a)

    with pytest.raises(PubSubError):
        hubtst.hub.subscribe(hubtst.call_a, hubtst.call_a)


def test_hub_has_topic(hubtst):
    assert hubtst.hub.has("topic.one")


def test_hub_clear_all(hubtst):
    assert len(hubtst.hub._events) == 3
    hubtst.hub.clear_all()
    assert len(hubtst.hub._events) == 0


def test_hub_topic_list(hubtst):
    topic_list = hubtst.hub.topic_list()
    assert topic_list == hubtst.topics


def test_hub_empty_hub(hubtst):
    topic = "topic.one"
    hubtst.hub.clear_all()

    hubtst.hub.send_message(topic)
    assert len(hubtst.hub._missing_topics) == 1
    assert hubtst.hub._missing_topics[topic] == 1

    hubtst.hub.send_message(topic)
    assert len(hubtst.hub._missing_topics) == 1
    assert hubtst.hub._missing_topics[topic] == 2

    assert hubtst.hub.missing_topic_list(), [topic]


def test_hub_add_topic(hubtst):
    hubtst.hub.subscribe(hubtst.call_a, "topic.one")
    assert hubtst.hub.has("topic.one")

    hubtst.hub.send_message("topic.one")
    assert hubtst.call_a.global_variable[0] == "AAA"


def test_hub_add_multiple_subscribers(hubtst):
    hubtst.hub.subscribe(hubtst.call_a, "topic.one")
    hubtst.hub.subscribe(hubtst.call_b, "topic.one")

    hubtst.hub.send_message("topic.one")

    assert hubtst.call_a.global_variable[0] == "AAABBB"
    assert hubtst.call_b.global_variable[0] == "AAABBB"


def test_hub_event_raise_exception(hubtst):
    def error():
        raise Exception("Uh oh")

    hubtst.hub.subscribe(error, "errors")

    with pytest.raises(EventError):
        hubtst.hub.send_message("errors")
