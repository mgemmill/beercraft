from beercraft.helpers import expose


def create_error_handler(obj):
    def error_handler(ex):
        obj.set_global('ERRORCAPTURE: ')
    return error_handler


def unbound_method(value):
    return value


class BasicObject(object):

    def __init__(self):
        pass

    def bound_method(self, value):
        return value

    def __call__(self, value):
        return value


class NewCallableObject(object):

    global_variable = ['']

    def __init__(self, name=''):
        self.name = name

    @property
    def get_global_var(self):
        return self.global_variable[0]

    @classmethod
    def clear_global(cls):
        cls.global_variable[0] = ''

    @classmethod
    def set_global(cls, value):
        cls.global_variable[0] += value

    def __call__(self, *args, **kwargs):
        self.set_global(self.name)

    def method(self, *args, **kwargs):
        self(*args, **kwargs)


class NewCallableObjectWithArgs(NewCallableObject):

    def __call__(self, input):
        self.set_global('{0}'.format(input))


class NewCallableObjectWithError(NewCallableObject):

    def __call__(self, *args, **kwargs):
        raise Exception('Exception was raised!')


class Commands(NewCallableObject):

    def __init__(self):
        super(Commands, self).__init__()

    @expose('commands.call')
    def call_a(self):
        print('call_a...')
        self.set_global('AAA')

    @expose('commands.call')
    def call_b(self):
        print('call_b...')
        self.set_global('BBB')

    @expose('commands.call')
    def call_c(self):
        print('call_c...')
        self.set_global('CCC')


class ClassCommands(NewCallableObject):

    @expose('classmethod.call')
    @classmethod
    def call_class_method(cls):
        cls.global_variable[0] += 'CLASS METHOD'

    @expose('staticmethod.call')
    @staticmethod
    def call_static_method():
        ClassCommands.global_variable[0] += 'STATIC METHOD'
