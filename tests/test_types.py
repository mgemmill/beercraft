import inspect
from beercraft.types import (
    apply_ref,
    bound,
    has_self_arg,
    callable_class,
    acceptable,
    requires_weakref,
)


def standard_function():
    pass


class NonCallableClass(object):
    def __init__(self):
        pass

    def method(self):
        pass

    @classmethod
    def class_method(cls):
        pass

    @staticmethod
    def static_method():
        pass


class CallableClass(object):
    def __init__(self):
        pass

    def __call__(self):
        pass

    def method(self):
        pass

    @classmethod
    def class_method(cls):
        pass

    @staticmethod
    def static_method():
        pass


ncc = NonCallableClass()
cc = CallableClass()


def test_bound():

    assert bound(standard_function) is False
    assert bound(CallableClass.method) is False
    assert bound(CallableClass.class_method) is True
    assert bound(CallableClass.static_method) is False
    assert bound(CallableClass.__call__) is False

    cc = CallableClass()
    assert bound(cc) is False
    assert bound(cc.method) is True
    assert bound(cc.class_method) is True
    assert bound(cc.static_method) is False
    assert bound(cc.__call__) is True


def test_has_self_arg():

    assert has_self_arg(standard_function) is False

    print(inspect.getfullargspec(NonCallableClass))
    print(inspect.getfullargspec(CallableClass))

    # any class with an __init__ will have a self arg
    assert has_self_arg(CallableClass) is False
    assert has_self_arg(CallableClass.method) is True
    assert has_self_arg(CallableClass.class_method) is False
    assert has_self_arg(CallableClass.static_method) is False
    assert has_self_arg(CallableClass.__call__) is True

    cc = CallableClass()
    assert has_self_arg(cc) is True
    assert has_self_arg(cc.method) is True
    assert has_self_arg(cc.class_method) is False
    assert has_self_arg(cc.static_method) is False
    assert has_self_arg(cc.__call__) is True


def test_callable_class():

    assert bound(NonCallableClass) is False
    assert inspect.isfunction(NonCallableClass) is False
    assert has_self_arg(NonCallableClass) is False
    assert callable_class(NonCallableClass) is False


def test_requires_weakref():

    assert bound(standard_function) is False
    assert has_self_arg(standard_function) is False
    assert callable_class(standard_function) is False
    assert requires_weakref(standard_function) is False

    assert bound(NonCallableClass) is False
    assert has_self_arg(NonCallableClass) is False
    assert callable_class(NonCallableClass) is False
    assert requires_weakref(NonCallableClass) is False

    inst = NonCallableClass()
    assert bound(inst) is False
    assert isinstance(inst, type) is False
    assert has_self_arg(inst) is False
    assert callable_class(inst) is False
    assert requires_weakref(inst) is False


ACCEPTABLE = True
NON_ACCEPT = False
WEAKMETH = "WeakMethod"
WEAKREF = "weakref"
NO_WEAKREF = None

EXPECTATIONS = [  # noqa
    (standard_function, ACCEPTABLE, WEAKREF, "function"),  # noqa
    # Class
    (NonCallableClass, NON_ACCEPT, NO_WEAKREF, "non-callable class"),  # noqa
    (
        NonCallableClass.method,
        NON_ACCEPT,
        NO_WEAKREF,
        "unbound non-callable class method",
    ),  # noqa
    (
        NonCallableClass.class_method,
        ACCEPTABLE,
        WEAKMETH,
        "non-callable class class method",
    ),  # noqa
    (
        NonCallableClass.static_method,
        ACCEPTABLE,
        WEAKREF,
        "static non-callable class function",
    ),  # noqa
    # instance
    (ncc, NON_ACCEPT, NO_WEAKREF, "non-callable class instance"),  # noqa
    (ncc.method, ACCEPTABLE, WEAKMETH, "non-callable class instance method"),  # noqa
    (
        ncc.class_method,
        ACCEPTABLE,
        WEAKMETH,
        "non-callable class instance class method",
    ),  # noqa
    (
        ncc.static_method,
        ACCEPTABLE,
        WEAKREF,
        "non-callable class instance static method",
    ),  # noqa
    # Class
    (CallableClass, NON_ACCEPT, NO_WEAKREF, "callable class"),  # noqa
    (
        CallableClass.method,
        NON_ACCEPT,
        NO_WEAKREF,
        "unbound callable class method",
    ),  # noqa
    (
        CallableClass.class_method,
        ACCEPTABLE,
        WEAKMETH,
        "callable class class method",
    ),  # noqa
    (
        CallableClass.static_method,
        ACCEPTABLE,
        WEAKREF,
        "static callable class func",
    ),  # noqa
    # instance
    (cc, ACCEPTABLE, WEAKREF, "callable class instance"),  # noqa
    (cc.method, ACCEPTABLE, WEAKMETH, "class instance method"),  # noqa
    (
        cc.class_method,
        ACCEPTABLE,
        WEAKMETH,
        "callable class instance class method",
    ),  # noqa
    (
        cc.static_method,
        ACCEPTABLE,
        WEAKREF,
        "callable class instance static method",
    ),  # noqa
]


def test_weakmethod():
    for obj, is_acceptable, weakref_type, desc in EXPECTATIONS:
        print("{} -> {}".format(desc, obj))
        assert acceptable(obj) is is_acceptable
        if is_acceptable:
            wr = apply_ref(obj)
            assert type(wr).__name__ == weakref_type
