from beercraft import expose


global_variable = [""]


@expose("module.call")
def function_a(value="AAA"):
    global_variable[0] += value


@expose("module.call")
def function_b(value="BBB"):
    global_variable[0] += value


@expose("module.call")
def function_c(value="CCC"):
    global_variable[0] += value


def clear_global():
    global_variable[0] = ""
