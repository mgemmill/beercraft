import weakref
import gc


class A(object):
    def __call__(self):
        print("A()")

    def instance(self):
        print("A.instance()")

    @classmethod
    def clsmth(csl):
        print("A.clsmth()")

    @staticmethod
    def staticmth():
        print("A.staticmth()")


a = A()

ref = weakref.WeakMethod(a.instance)

print("WeakMethod reference: {}".format(ref()))
print("Calling reference:")
ref()()

print("deleting instance `a`....")
del a
print("collecting garbage....")
gc.collect()

print("WeakMethod reference: {}".format(ref()))


refb = weakref.WeakMethod(A.clsmth)
print("WeakMethod class reference: {}".format(refb()))
refb()()

# can't do this with a static function
#  refc = weakref.WeakMethod(A.staticmth)
#  print('WeakMethod static reference: {}'.format(refc()))
#  refc()()

d = A()
refd = weakref.WeakMethod(d)
print("WeakMethod class reference: {}".format(refd()))
refd()()
