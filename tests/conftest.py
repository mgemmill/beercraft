from types import SimpleNamespace
import pytest
from beercraft.hub import Hub
from beercraft.event import Event
from .tutils import create_error_handler
from .tutils import NewCallableObject
from .tutils import NewCallableObjectWithArgs
from .tutils import NewCallableObjectWithError


@pytest.fixture
def hubtst():

    obj = SimpleNamespace()
    obj.topics = topics = ["topic.one", "topic.two", "topic.three"]
    obj.call_a = NewCallableObject("AAA")
    obj.call_b = NewCallableObject("BBB")
    obj.call_c = NewCallableObject("CCC")

    obj.hub = Hub()
    for t in topics:
        obj.hub.add_topic(t)

    obj.call_a.clear_global()
    return obj


@pytest.fixture
def objects():
    obj = SimpleNamespace()
    obj.call_a = NewCallableObject("AAA")
    obj.call_b = NewCallableObject("BBB")
    obj.call_c = NewCallableObject("CCC")

    obj.event = event = Event("test event")

    event.register(obj.call_a.method)
    event.register(obj.call_b.method)
    event.register(obj.call_c.method)

    obj.call_a.clear_global()
    return obj


@pytest.fixture
def objargs():
    obj = SimpleNamespace()
    obj.call_a = NewCallableObjectWithArgs("AAA")
    obj.call_b = NewCallableObjectWithArgs("BBB")
    obj.call_c = NewCallableObjectWithArgs("CCC")

    obj.event = event = Event("test event")

    event.register(obj.call_a.method)
    event.register(obj.call_b.method)
    event.register(obj.call_c.method)

    obj.call_a.clear_global()
    return obj


@pytest.fixture
def objerr():
    obj = SimpleNamespace()
    obj.call_a = NewCallableObjectWithError("AAA")
    obj.call_b = NewCallableObject("BBB")
    obj.call_c = NewCallableObject("CCC")
    obj.error_handler = create_error_handler(obj.call_a)

    obj.event = event = Event("test event")

    event.register(obj.call_a.method)
    event.register(obj.call_b.method)
    event.register(obj.call_c.method)

    obj.call_a.clear_global()
    return obj
