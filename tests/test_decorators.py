"""Unit tests for the testing utilities.
"""
import pytest
from beercraft import pub
from beercraft import expose, subscribe_commands, auto_subscribe
from .tutils import NewCallableObject


class UnWrappedClass(NewCallableObject):
    def __init__(self, pub_id):
        super(UnWrappedClass, self).__init__()
        self.__pub_id__ = pub_id

    @expose("{0}.event")
    def call_dynamic_event(self):
        self.set_global("A")

    @expose("static.event")
    def call_static_event(self):
        self.set_global("B")


@auto_subscribe()
class WrappedClass(NewCallableObject):
    def __init__(self, pub_id):
        super(WrappedClass, self).__init__()
        self.__pub_id__ = pub_id

    @expose("{0}.event")
    def call_dynamic_event(self):
        self.set_global("A")

    @expose("static.event")
    def call_static_event(self):
        self.set_global("B")


@auto_subscribe("wrappedclass")
class WrappedClassAutoID(NewCallableObject):
    def __init__(self, pub_id):
        super(WrappedClassAutoID, self).__init__()

    @expose("{0}.event")
    def call_dynamic_event(self):
        self.set_global("A")


def test_undecorated_class_object():

    pub.clear_all()

    obj = UnWrappedClass("foo")

    subscribe_commands(obj)

    pub.send_message("static.event")
    assert obj.get_global_var == "B"

    obj.clear_global()
    pub.send_message("foo.event")

    assert obj.get_global_var == "A"

    obj.clear_global()
    pub.clear_all()


def test_decorated_class_object():

    pub.clear_all()

    obj = WrappedClass("foo")

    pub.send_message("static.event")
    assert obj.get_global_var == "B"

    obj.clear_global()
    pub.send_message("foo.event")

    assert obj.get_global_var == "A"

    obj.clear_global()
    pub.clear_all()


def test_subscribing_decorated_class_object():

    pub.clear_all()

    obj = WrappedClass("foo")

    with pytest.warns(RuntimeWarning):
        subscribe_commands(obj)

    pub.send_message("static.event")
    assert obj.get_global_var == "B"

    obj.clear_global()
    pub.send_message("foo.event")

    assert obj.get_global_var == "A"

    obj.clear_global()
    pub.clear_all()


def test_subscribing_decorated_class_object_with_auto_id():

    pub.clear_all()

    oid = pub.oid.new("wrappedclass")

    obj = WrappedClassAutoID("foo")

    assert len(str(obj.__pub_id__)) == 36

    obj.clear_global()
    pub.send_message("{}.event".format(oid))

    assert obj.get_global_var == "A"

    obj.clear_global()
    pub.clear_all()
