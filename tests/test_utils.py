"""Unit tests for the testing utilities.
"""
from types import SimpleNamespace
import pytest
from beercraft.hub import Hub
from beercraft.helpers import topic_formatter, expose, subscribe_commands, PubSubError
from .tutils import Commands


pub = Hub()


class BogusObject(object):
    pass


class DynamicCommand(Commands):
    def __init__(self, pub_id):
        super(DynamicCommand, self).__init__()
        self.__pub_id__ = pub_id

    @expose("{0}.test")
    def call_d(self):
        print("call_d...")
        self.set_global("DDD")


@pytest.fixture
def cmdtst():
    obj = SimpleNamespace()
    obj.commands = Commands()
    obj.commands.clear_global()

    pub.clear_all()

    return obj


def test_pub_empty(cmdtst):
    assert len(pub.topic_list()) == 0


def test_subscribe_commands(cmdtst):
    subscribe_commands(cmdtst.commands)
    assert len(pub.topic_list()) == 1
    assert list(pub.topic_list())[0] == "commands.call"


def test_call_commands(cmdtst):
    print("test_call_commands call")
    subscribe_commands(cmdtst.commands)
    assert len(pub.topic_list()) == 1
    assert pub.topic_list()[0] == "commands.call"

    print("unittest pubsub id: {0}".format(id(pub)))
    pub.send_message("commands.call")


def assert_pub_state():
    assert len(pub.topic_list()) == 2
    print(pub.topic_list())
    assert "commands.test" in pub.topic_list()
    assert "commands.call" in pub.topic_list()


def test_call_dynamic_topic_asignment(cmdtst):
    commands = DynamicCommand("commands")
    subscribe_commands(commands)
    assert_pub_state()


def test_method_topic_type():
    commands = DynamicCommand("commands")
    assert callable(commands.call_d.topic)


def test_null_pubsub_it():
    commands = DynamicCommand(None)
    with pytest.raises(PubSubError):
        subscribe_commands(commands)
    assert_pub_state()


def test_topic_formatter_with_subs():

    tpc = topic_formatter("{}.string")

    with pytest.raises(PubSubError):
        tpc()

    assert tpc("substitute") == "substitute.string"


def test_expose_basic_usage():
    @expose("test.topic")
    def func():
        pass

    assert func.pubsub is True


def test_expose_with_topic_error():
    with pytest.raises(PubSubError):

        @expose(345)
        def func():
            pass


def test_expose_with_custom_logger_error():
    class Logger(object):
        pass

    with pytest.raises(PubSubError):

        @expose("test.topic", logger=Logger())
        def func():
            pass


def test_expose_with_custom_logger():
    class Logger(object):
        logged_value = ""

        def debug(self, input):
            pass

    logger = Logger()

    @expose("test.topic", logger=logger)
    def func():
        pass

    func()
