import os
from glob import glob
from setuptools import setup
from setuptools import find_packages
from os.path import basename, splitext

__version__ = '0.1.0'

readme_file = os.path.join(os.path.dirname(__file__), 'README.rst')
long_description = open(readme_file, 'r').read()


setup(name='beercraft',
      version=__version__,
      author='Mark Gemmill',
      author_email='dev@markgemmill.com',
      license='Apache Software License',
      packages=find_packages(where='src'),
      package_dir={'': 'src'},
      py_modules=[splitext(basename(i))[0] for i in glob("src/*.py")],
      zip_safe=False,
      test_suite='tests',
      test_requirements=['pytest'])
