import names
import random


def get_number():
    area = random.choice(("604", "778"))
    seg_1 = random.randint(100, 900)
    seg_2 = random.randint(1000, 9000)
    return "{}-{}-{}".format(area, seg_1, seg_2)


PHONEBOOK = []

print("building phonebook....")

for i in range(0, 1000):
    # create a phonebook listing of random names and numbers
    first = names.get_first_name()
    last = names.get_last_name()
    phone = get_number()
    PHONEBOOK.append(("{}. {}".format(first[0], last), phone))

print("phonebook is ready for telemarketers!")
