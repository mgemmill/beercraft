from time import sleep
from beercraft import pub
from phonebook import PHONEBOOK
import random
import backend


class Telemarketer(object):
    def __init__(self, name):
        self.name = name

    def receive_answer(self, **kwargs):
        kwargs["telemarketer"] = self.name
        reply = "{telemarketer} called {number} and {callee} said: {answer}"
        print(reply.format(**kwargs))

    def no_answer(self, **kwargs):
        kwargs["telemarketer"] = self.name
        reply = "{telemarketer} did not get through to {number}."
        print(reply.format(**kwargs))


telemarketers = [
    Telemarketer("phoneco"),
    Telemarketer("travelco"),
    Telemarketer("realestateco"),
    Telemarketer("marketingco"),
]


def main():

    backend.load_customer()

    for tm in telemarketers:
        # register the topic channel for call responses from customers
        # if there is no answer...
        pub.subscribe(tm.no_answer, "{}.no-answer".format(tm.name))
        # or if they do answer...
        pub.subscribe(tm.receive_answer, "{}.answer".format(tm.name))

    for name, number in PHONEBOOK:
        pub.add_topic(number)

    while True:
        # select a random number and telemarketer to make a call...
        name, number = random.choice(PHONEBOOK)
        telemarketer = random.choice(telemarketers)
        # telemarketers calls the number
        pub.send_message(number, telemarketer=telemarketer.name)
        sleep(2)  # so we can read what is happening


if __name__ == "__main__":
    main()
