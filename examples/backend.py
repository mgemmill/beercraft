from beercraft import pub
from phonebook import PHONEBOOK
import random


class PotentialCustomer(object):
    def __init__(self, name, number):
        self.name = name
        self.number = number

    def answer_call(self, **kwargs):
        tm = kwargs.get("telemarketer", "")
        pickup = random.choice((True, False))

        if not pickup:
            # If customer does not pick up, send message
            # back on telemarketers 'no-answer' topic.
            pub.send_message(tm + ".no-answer", number=self.number)
            return

        # if the customer does answer, send the
        # reply back on the telemarketers 'answer' topic channel.
        reply = random.choice(
            ("Thank you for the info!", "#@!%* you!", "CLICK....zzzzz")
        )
        pub.send_message(
            tm + ".answer", number=self.number, callee=self.name, answer=reply
        )


def load_customer():

    customers = []

    for name, number in PHONEBOOK:
        customer = PotentialCustomer(name, number)
        # register customer to answer calls on their number
        pub.subscribe(customer.answer_call, number)
        customers.append(customer)

    return customers
