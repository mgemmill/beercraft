beercraft v0.1.0
================

``beercraft`` is a python *pubsub* (publish/subscribe) library for
loosely coupled syncronous in-app communications. It's
particularly useful for gui applications written
with the likes of wxPython, PySide, etc. but
can be used in an situation that warrants the
need to keep caller's and callee's annonymous.


Installation
------------

.. code:: bash


    ... pip install beercraft


Basic Usage
-----------

.. code:: python

    # backend.py

    from beercraft import pub

    # here we

    def handle_gui_event(data=None):
        print(f'handling {data}')
        # and do whatever else needs
        # to be done with the data....


    pub.subscribe(handle_gui_event, 'gui.save')


.. code:: python

    # gui.py
    from beercraft import pub

    # Somewhere, nested in the bowels of gui code
    # is the event handler that responds to the
    # save button's click event.

    def save_button_event(evt):
        # send the data to the backend code you want to
        # handle the saved data.
        pub.send_message('gui.save', data={'message': 'Hola!'})
